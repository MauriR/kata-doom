const MAX_HEALTH_POINTS = 100
const MAX_ARMOR_POINTS = 50

class Player {
  constructor() {
    this.healthPoints = MAX_HEALTH_POINTS
    this.armorPoints = MAX_ARMOR_POINTS
    this.weapon = 'Fists'
    this.arsenal = ['Fists']
    this.arsenalCapacity = 5
  }

  obtainHealthPoints() {
    return this.healthPoints
  }

  obtainArmorPoints() {
    return this.armorPoints
  }

  obtainWeapon() {
    return this.weapon
  }

  obtainArsenalCapacity() {
    return this.arsenalCapacity
  }

  addWeapon(weapon) {
    for (let item of this.arsenal) {
      if (weapon === item) {
        return 'Error'
      }
    }
    if (this.weaponCapacityNotReached()) {
      this.arsenal.push(weapon)
    }
  }

  weaponCapacityNotReached() {
    return this.arsenal.length < this.arsenalCapacity
  }

}

module.exports = Player