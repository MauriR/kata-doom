const Player = require('./Player')

describe('Player', () => {
  it('gives main character a maximum health points', () => {
    const player = new Player()
    const initialHealthPoints = 100

    const maxHP = player.obtainHealthPoints()

    expect(maxHP).toBe(initialHealthPoints)
  })

  it('gives main character a maximum armor points', () =>{
    const player = new Player()
    const initialArmorPoints = 50

    const maxArmorPoints = player.obtainArmorPoints()

    expect(maxArmorPoints).toBe(initialArmorPoints)
  })

  it('gives main character fists as their initial weapon', ()=> {
    const player = new Player()
    const initialWeapon = 'Fists'

    const maxArmorPoints = player.obtainWeapon()

    expect(maxArmorPoints).toBe(initialWeapon)
  })

  it('gives main character at the beginning a maximum  number of weapon', ()=> {
    const player = new Player()
    const maxCapacity = 5
    player.arsenal = ['Fists', 'Gunshot', 'Machine Gun', 'Stone', 'Broom']
    
    player.addWeapon('Machete')


    expect(player.arsenal.length).toBe(maxCapacity)
  })

  it ('does not allow character to own two equal weapons', () => {
    const player = new Player()
    
    player.addWeapon('Machete')
    player.addWeapon('Gunshot')

    const checkArsenal = player.addWeapon('Machete')
    
    expect(checkArsenal).toBe('Error')

  })

})

